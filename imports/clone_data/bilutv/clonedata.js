import cheerio from 'cheerio';
import request from 'request';
import aes from 'gibberish-aes';

function doRequest(url) {
  return new Promise(function (resolve, reject) {
    request(url, function (error, res, body) {
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        reject(error);
      }
    });
  });
}

export const getUrlPlay = async (url)=>{
    let body = await doRequest(url)
    let $ = cheerio.load(body)
    let script = $('.left-content-player > script').html()
    let regex = /{"sourcesTm":.*(\d*})/g
    var match = regex.exec(script);
    let json = []
    if(match != undefined){
       
        json =getObLinkPlay(JSON.parse(match[0]))
    }
    return json
}
getPass = (modelId)=>{
    return  `bilutv.com4590481877${modelId}`
}
getObLinkPlay = (ob)=>{
    let json = {}
    let modelId = ob.modelId
    json.poster = ob.poster
    json.title = ob.title
    if(ob.sourcesTm != undefined){
        json.sourcesTm = getSource(ob.sourcesTm,modelId)
    }
    if(ob.sourcesTm != undefined){
        json.sourcesVs = getSource(ob.sourcesVs,modelId)
    }
    return json
}
getSource = (source,modelId)=>{
    let ss = []
    source.forEach(function(e) {
        let s = {}
        s.server = e.server
        s.label = e.label
        s.links =  getLink(e.links,modelId)
        ss.push(s)
    });
    return ss
}
getLink=  (links,modelId)=>{
    let ls = []
    links.forEach(function(link) {
        let l = {}
        l.type = link.type
        l.label = link.label
        l.default = link.default
        l.file =  aes['dec'](link.file,getPass(modelId))
        ls.push(l)
    });
    return ls
}
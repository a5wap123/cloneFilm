import React, { Component, PropTypes } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import ReactDOM from 'react-dom';
// App component - represents the whole app

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            json: undefined
        }
    }
    componentDidMount() {

    }
    handleSubmit(event) {
        event.preventDefault();
        // Find the text field via the React ref
        const url = ReactDOM.findDOMNode(this.refs.textInput).value.trim();

        Meteor.call('get', url, (error, result) => {
            if (error) {
                // handle error
            }
            else {

                this.setState({ json: result })
            }
        })

    }
    render() {
        return (
            <div className="container">
                <header>
                    <h1>Clone Phim</h1>
                    <form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
                        <input
                            type="text"
                            ref="textInput"
                            placeholder="Link xem film..."
                            defaultValue='http://bilutv.com/xem-phim/phim-tung-dot-song-vo-my-mr.mermaid-2017-5406'
                            style={{width:500}}
                        />
                    </form>
                </header>
                <div>
                    {this.state.json ?
                        <div>
                            <img src={this.state.json.poster} style={{ width: 680, height: 410 }} />
                            <p>{this.state.json.title}</p>
                        </div> : ''
                    }
                    <br />

                    {JSON.stringify(this.state.json)}
                </div>
            </div>
        );
    }
}
